package com.baskara.geotaggingsungai;

import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class DetailRiverActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView textViewPostedBy, textViewRiverActivity, textViewRiverLocation, textViewRiverCondition,
            textViewRiverDescription, likeCount, commentCount, timeStamp;
    private String TAG = "DetailRiverActivity";
    private DatabaseReference mDatabase =  FirebaseDatabase.getInstance().getReference();
    private StorageReference mStorage = FirebaseStorage.getInstance().getReference();
    private DatabaseReference databaseReferenceSelectedRiver;
    private StorageReference imageRef;
    private ImageView imageViewRiver;
    private CircleImageView circleImageViewProfile;
    private CheckBox likeIcon;
    private ImageView commentIcon;
    private int toogleLike;
    private String uuidCurrentUser;
    private ArrayList<String> arrayListLike = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_river);
        initialize();
        readUserData();
        readRiverData();
        showRiverPicture(getUuid());
    }
    private void initialize(){
        timeStamp = findViewById(R.id.timeStamp);
        imageViewRiver = findViewById(R.id.imageViewRiver);
        textViewPostedBy = findViewById(R.id.textViewPostedBy);
        textViewRiverActivity = findViewById(R.id.textViewRiverActivity);
        textViewRiverLocation = findViewById(R.id.textViewRiverLocation);
        textViewRiverCondition = findViewById(R.id.textViewRiverCondition);
        commentCount = findViewById(R.id.commentCount);
        likeCount = findViewById(R.id.likeCount);
        textViewRiverDescription = findViewById(R.id.textViewRiverDescription);
        circleImageViewProfile = findViewById(R.id.circleImageViewProfile);
        likeIcon = findViewById(R.id.likeIcon);
        commentIcon = findViewById(R.id.commentIcon);
        likeIcon.setOnClickListener(this);
        commentIcon.setOnClickListener(this);
        commentCount.setOnClickListener(this);
    }
    private void readRiverData(){
        final FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        DatabaseReference mRiver = FirebaseDatabase.getInstance().getReference("river");
        mRiver.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River river = snapshot.getValue(River.class);
                    if (river.getLatitude() == getLatitude() && river.getLongitude() == getLongitude()
                            && river.getUuid().equalsIgnoreCase(getUuid())){
                        textViewRiverCondition.setText(river.getKondisiSungai());
                        textViewRiverDescription.setText(river.getRiverDescription());
                        textViewRiverLocation.setText(river.getRiverLocation());
                        textViewRiverCondition.setText(river.getKondisiSungai());
                        showRiverActivities(river.getArrayListRiverActivity());
                        showTimeStamp(river.getTimeStamp());
                        if (!river.getArrayListLike().isEmpty()){
                            arrayListLike = river.getArrayListLike();
                            if (arrayListLike.contains(currentFirebaseUser.getUid())){
                                likeIcon.setChecked(true);
                            }
                        }
                        databaseReferenceSelectedRiver = snapshot.getRef();
                    }
                }
                showLikeCount();
                showCommentCount();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void showTimeStamp(Map<String, Object> map){
        String time = map.get("timestamp").toString();
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(Long.parseLong(time));
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        timeStamp.setText(date);
    }
    private void showCommentCount(){
        DatabaseReference commentDatabaseRef = databaseReferenceSelectedRiver.child("comment");
        commentDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int size = (int) dataSnapshot.getChildrenCount();
                if (size==0){
                    commentCount.setText("No comment yet");
                }
                else if (size<2){
                    commentCount.setText("View "+size+" Comment");
                }
                else{
                    commentCount.setText("View "+size+" Comments");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void showLikeCount(){
        if (!arrayListLike.isEmpty()){
            int count = arrayListLike.size();
            if (count<2){
                likeCount.setText(count+" Like");
            }
            else{
                likeCount.setText(count+" Likes");
            }
        }
        else {
            likeCount.setText("0 Like");
        }
    }
    private void readUserData(){
        DatabaseReference userDatabase = FirebaseDatabase.getInstance().getReference("Users");
        DatabaseReference userDatabaseByUserID = userDatabase.child(getUserID());
        userDatabaseByUserID.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                textViewPostedBy.setText(user.getUsername());
                uuidCurrentUser = user.getUuid();
                showProfilePicture(user.getUuid());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(DetailRiverActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void showProfilePicture(String uuid){
        if (uuid.equalsIgnoreCase("default")){
            circleImageViewProfile.setImageResource(R.drawable.default_picture);
        }
        else{
            imageRef = mStorage.child(uuid);
            imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Uri url = uri;
                    Picasso.with(getApplicationContext()).load(url).into(circleImageViewProfile);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });
        }
    }
    private void showRiverActivities(ArrayList arrayListRiverActivity){
        String activities ="";
        if (arrayListRiverActivity!= null){
            for (int i=0; i < arrayListRiverActivity.size(); i++){
                int count = arrayListRiverActivity.size() - i;
                if (count == 1){
                    activities += arrayListRiverActivity.get(i);
                }
                else {
                    activities += arrayListRiverActivity.get(i)+" . ";
                }
            }
        }
        textViewRiverActivity.setText(activities);
    }
    private void showRiverPicture(String uuid){
            imageRef = mStorage.child(uuid);
            imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Uri url = uri;
                    Picasso.with(getApplicationContext()).load(url).into(imageViewRiver);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });
    }
    private double getLatitude(){
        Bundle extras = getIntent().getExtras();
        double latitude = 0;
        if (extras!=null){
            latitude = extras.getDouble("latitude");
        }
        return latitude;
    }
    private double getLongitude(){
        Bundle extras = getIntent().getExtras();
        double longitude = 0;
        if (extras!=null){
            longitude = extras.getDouble("longitude");
        }
        return longitude;
    }
    private String getUuid(){
        Bundle extras = getIntent().getExtras();
        String uuid = "";
        if (extras!=null){
            uuid = extras.getString("uuid");
        }
        return uuid;
    }
    private String getUserID(){
        Bundle extras = getIntent().getExtras();
        String userID = "";
        if (extras!=null){
            userID = extras.getString("userID");
        }
        return userID;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.likeIcon:
                if (likeIcon.isChecked()){
                    if (!arrayListLike.contains(getUserID())){
                        arrayListLike.add(getUserID());
                    }
                    editLike();
                }
                else{
                    if (arrayListLike.contains(getUserID())){
                        arrayListLike.remove(getUserID());
                    }
                    editLike();
                }
        }
        if (view == commentIcon){
            goToCommentActivity(getUserID(), databaseReferenceSelectedRiver);
        }
        if (view == commentCount){
            goToCommentActivity(getUserID(), databaseReferenceSelectedRiver);
        }
    }
    private void editLike(){
        final DatabaseReference mRiver = FirebaseDatabase.getInstance().getReference("river");
        mRiver.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River river = snapshot.getValue(River.class);
                    if (river.getLatitude() == getLatitude() && river.getLongitude() == getLongitude()
                            && river.getUuid().equalsIgnoreCase(getUuid())){
                        DatabaseReference databaseReference = snapshot.getRef();
                        databaseReference.child("arrayListLike").setValue(arrayListLike);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void goToCommentActivity(String userID, DatabaseReference databaseReferenceSelectedRiver){
        Intent intent = new Intent(getApplicationContext(), CommentActivity.class);
        intent.putExtra("userID", userID);
        intent.putExtra("databaseReferenceSelectedRiver", databaseReferenceSelectedRiver.toString());
        startActivity(intent);
    }
}
