package com.baskara.geotaggingsungai;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class FormGeotaggingActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = "FormGeotaggingActivity";
    private EditText editTextKondisi;
    private TextInputLayout input_layout_kondisi;
    private TextInputLayout input_layout_aktifitas;
    private TextInputLayout input_layout_camera;
    private ImageView imageViewCurrentPhoto;
    private FloatingActionButton floatingActionButtonTakePhoto;
    private Button buttonSubmit;
    private CheckBox checkBoxMemancing;
    private CheckBox checkBoxPembersihan;
    private ArrayList<String> arrayListPost;
    private CheckBox checkBoxBerenang;
    ArrayList<String> arrayListRiverActivity = new ArrayList<String>();
    public ArrayList<String> arrayListLike = new ArrayList<>();
    private DatabaseReference mDatabase =  FirebaseDatabase.getInstance().getReference();
    FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private StorageReference mStorage = FirebaseStorage.getInstance().getReference();
    String uuid = UUID.randomUUID().toString();
    private static final int CAMERA_REQUEST_CODE=1;
    private String currentUsername;
    private String currentAchievement;
    private int currentPoin;
    private int currentLevel;
    private Bitmap mImageUri;
    private String uuidUserPhoto;
    private int postCounter;
    private String activityMemancing;
    private String activityBerenang;
    private String activityPembersihan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_geotagging);
        initialize();
        initializeCheckbox();
        checkCameraPermission();
        readUserData();
        setGenerateName(uuid);
    }
    private void initialize(){
        editTextKondisi = findViewById(R.id.editTextKondisi);
        buttonSubmit = findViewById(R.id.buttonSubmit);
        imageViewCurrentPhoto = findViewById(R.id.imageViewCurrentPhoto);
        floatingActionButtonTakePhoto = findViewById(R.id.floatingActionButtonTakePhoto);
        input_layout_aktifitas = findViewById(R.id.input_layout_aktifitas);
        input_layout_kondisi = findViewById(R.id.input_layout_kondisi);
        input_layout_camera = findViewById(R.id.input_layout_camera);
        floatingActionButtonTakePhoto.setOnClickListener(this);
        buttonSubmit.setOnClickListener(this);
    }
    private void initializeCheckbox(){
        checkBoxPembersihan = findViewById(R.id.checkBoxPembersihan);
        checkBoxBerenang = findViewById(R.id.checkBoxBerenang);
        checkBoxMemancing = findViewById(R.id.checkBoxMemancing);
        checkBoxPembersihan.setOnClickListener(this);
        checkBoxBerenang.setOnClickListener(this);
        checkBoxMemancing.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.checkBoxMemancing:
                if (checkBoxMemancing.isChecked()){
                    if (!arrayListRiverActivity.contains("Memancing")){
                        arrayListRiverActivity.add("Memancing");
                    }
                }
                else {
                    if (arrayListRiverActivity.contains("Memancing")){
                        arrayListRiverActivity.remove("Memancing");
                    }
                }
                System.out.println("Ako"+arrayListRiverActivity);
            case R.id.checkBoxBerenang:
                if (checkBoxBerenang.isChecked()){
                    if (!arrayListRiverActivity.contains("Berenang")){
                        arrayListRiverActivity.add("Berenang");
                    }
                }
                else {
                    if (arrayListRiverActivity.contains("Berenang")){
                        arrayListRiverActivity.remove("Berenang");
                    }
                }
                System.out.println("Ako"+arrayListRiverActivity);
            case R.id.checkBoxPembersihan:
                if (checkBoxPembersihan.isChecked()){
                    if (!arrayListRiverActivity.contains("Pembersihan")){
                        arrayListRiverActivity.add("Pembersihan");
                    }
                }
                else {
                    if (arrayListRiverActivity.contains("Pembersihan")){
                        arrayListRiverActivity.remove("Pembersihan");
                    }
                }
                System.out.println("Ako"+arrayListRiverActivity);
        }
        if (view == buttonSubmit){
            submitForm();
        }
        if (view == floatingActionButtonTakePhoto){
            takePhotoFromIntent();
        }
    }
    private void takePhotoFromIntent(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, CAMERA_REQUEST_CODE);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            mImageUri = (Bitmap) data.getExtras().get("data");
            imageViewCurrentPhoto.setImageBitmap(mImageUri);
        }
    }
    private void saveRiverPhotoToFirebaseStorage(){
        StorageReference uploadFileRef = mStorage.child(uuid);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        mImageUri.compress(Bitmap.CompressFormat.PNG, 100, baos);
        //data upload to firebase
        byte[] data2 = baos.toByteArray();
        UploadTask uploadTask = uploadFileRef.putBytes(data2);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
            }
        });
    }

    private void geotaggingRiver(){
        writeGeotaggingRiver(getKondisiSungai(), arrayListRiverActivity, getLatitude(), getLongitude()
                , editTextKondisi.getText().toString(), currentFirebaseUser.getUid());
        geotaggingRiverSelection(getKondisiSungai());

    }
    private void geotaggingRiverSelection(String kondisiSungai){
        HashMap<String, Object> timestampNow = new HashMap<>();
        timestampNow.put("timestamp", ServerValue.TIMESTAMP);
        if (kondisiSungai.equalsIgnoreCase("Cemar berat")||kondisiSungai.equalsIgnoreCase("Cemar sedang")||
        kondisiSungai.equalsIgnoreCase("Cemar ringan")){
            River river = new River(kondisiSungai,arrayListRiverActivity,  getLatitude(), getLongitude(),
                    editTextKondisi.getText().toString(), currentFirebaseUser.getUid(), uuid, getRiverLocation(),
                    timestampNow, arrayListLike, uuidUserPhoto, currentUsername);
            mDatabase.child("dirtyriver").push().setValue(river);
            Log.d(TAG, "sungai kotor success submit");
        }
        else if (kondisiSungai.equalsIgnoreCase("Cukup baik")||kondisiSungai.equalsIgnoreCase("Sangat baik")){
            River river = new River(kondisiSungai,arrayListRiverActivity,  getLatitude(), getLongitude(),
                    editTextKondisi.getText().toString(), currentFirebaseUser.getUid(), uuid, getRiverLocation(),
                    timestampNow, arrayListLike, uuidUserPhoto, currentUsername);
            mDatabase.child("cleanriver").push().setValue(river);
            Log.d(TAG, "sungai bersih success submit");
        }
    }
    private void readUserData(){
        DatabaseReference userDatabase = FirebaseDatabase.getInstance().getReference("Users");
        final DatabaseReference userDatabaseByUserID = userDatabase.child(currentFirebaseUser.getUid());
        Log.d(TAG, currentFirebaseUser.getUid());
        userDatabaseByUserID.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                Log.d("testes", user.toString());
                if(user!=null){
                    currentUsername = user.getUsername();
                    currentAchievement = user.getCurrentAchievement();
                    currentLevel = user.getCurrentLevel();
                    currentPoin = user.getCurrentPoin();
                    uuidUserPhoto = user.getUuid();
                    postCounter = user.getPostCounter();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(FormGeotaggingActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private double getLatitude(){
        Bundle extras = getIntent().getExtras();
        double latitude = 0;
        if (extras!=null){
            latitude = extras.getDouble("latitude");
            Log.d(TAG, "get latitude"+extras.getDouble("latitude"));
        }
        return latitude;
    }
    private double getLongitude(){
        Bundle extras = getIntent().getExtras();
        double longitude = 0;
        if (extras!=null){
            longitude = extras.getDouble("longitude");
            Log.d(TAG, "get longitude"+extras.getDouble("longitude"));
        }
        return longitude;
    }
    private String getKondisiSungai(){
        Bundle extras = getIntent().getExtras();
        String kondisiSungai = "";
        if (extras!=null){
            kondisiSungai = extras.getString("kondisiSungai");
            Log.d(TAG, "get KondisiSungai"+extras.getString("kondisiSungai"));
        }
        return kondisiSungai;
    }
    private String getRiverLocation(){
        Bundle extras = getIntent().getExtras();
        String riverLocation = "";
        if (extras!=null){
            riverLocation = extras.getString("riverLocation");
        }
        return riverLocation;
    }
    private void checkCameraPermission(){
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED){
            int requestCode = 0;
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, requestCode);
        }
    }
    private void writeGeotaggingRiver(String kondisiSungai, ArrayList<String> arrayListRiverActivity, double latitude,
                                      double longitude, String riverDescription, String userID){
        HashMap<String, Object> timestampNow = new HashMap<>();
        timestampNow.put("timestamp", ServerValue.TIMESTAMP);
        River river = new River(kondisiSungai,arrayListRiverActivity,  latitude, longitude,
                riverDescription, userID, uuid, getRiverLocation(), timestampNow, arrayListLike, uuidUserPhoto, currentUsername);
        mDatabase.child("river").push().setValue(river);
        updateUserData();
        System.out.println("success write database");
    }
    private boolean checkIfArrayListRiverActivityIsEmpty(){
        boolean check = true;
        if (arrayListRiverActivity.isEmpty()){
            check = false;
        }
        System.out.println(check);
        return check;
    }
    private void submitForm() {
        if (!validateActivityRiver()) {
            return;
        }
        if (!validateKondisi()) {
            return;
        }
        if (!validateCamera()){
            return;
        }
        else{
            geotaggingRiver();
            saveRiverPhotoToFirebaseStorage();
        }
    }
    private boolean validateActivityRiver(){
        if (checkIfArrayListRiverActivityIsEmpty()== false){
            input_layout_aktifitas.setError(getString(R.string.err_msg_aktifitas));
            requestFocus(checkBoxPembersihan);
            return false;
        }
        return true;
    }
    private boolean validateCamera(){
        if (mImageUri == null){
            Toast.makeText(getApplicationContext(), R.string.err_msg_camera, Toast.LENGTH_SHORT).show();
            requestFocus(floatingActionButtonTakePhoto);
            System.out.println("camera null");
            return false;
        }
        return true;
    }
    private boolean validateKondisi() {
        if (editTextKondisi.getText().toString().trim().isEmpty()) {
            input_layout_kondisi.setError(getString(R.string.err_msg_kondisi));
            requestFocus(editTextKondisi);
            return false;
        } else {
            input_layout_kondisi.setErrorEnabled(false);
        }

        return true;
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private int resetPoin(int currentPoin){
        int newPoin;
        if (currentPoin >= 100){
            newPoin = 0;
        }
        else{
            newPoin = currentPoin;
        }
        return newPoin;
    }
    private void setGenerateName(final String uuid){
        this.uuid = uuid;
        StorageReference cekname = mStorage.child(uuid);
        if (!cekname.getName().equals(uuid)){
            String new_uuid = UUID.randomUUID().toString();
            setGenerateName(new_uuid);
        }
    }
    private void updateUserData(){
        Gamification gamification = new Gamification();
        int poin = currentPoin + gamification.getPoin(currentLevel);
        int level = currentLevel + gamification.getLevel(poin);
        String achievement = gamification.getAchievement(level);
        DatabaseReference userDatabase = FirebaseDatabase.getInstance().getReference("Users");
        DatabaseReference userDatabaseByUserID = userDatabase.child(currentFirebaseUser.getUid());
        int counter = postCounter+1;
        User user = new User(currentUsername, resetPoin(poin), level, achievement, uuidUserPhoto, counter);
        userDatabaseByUserID.setValue(user);
        Log.d(TAG, "update data success");
        goToAchievementActivity(achievement, resetPoin(poin), gamification.getPoin(currentLevel), level);
    }

    private void goToAchievementActivity(String achievement, int poin, int increasedPoin, int level){
        Intent intent = new Intent(getApplicationContext(), AchievementScreenActivity.class);
        intent.putExtra("achievement",achievement);
        intent.putExtra("poin", poin);
        intent.putExtra("increasedPoin", increasedPoin);
        intent.putExtra("level", level);
        startActivity(intent);
        finish();
    }
}
