package com.baskara.geotaggingsungai;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.security.PrivateKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HomeActivity extends AppCompatActivity{
    private BottomNavigationView bottomNavigationView;
    private List<Timeline> timelineList = new ArrayList<>();
    private RecyclerView recyclerView;
    private HomeAdapter mAdapter;
    private Timeline timeline;
    public String username;
    private CheckBox likeIcon;
    private ImageView imageView;
    private TextView textView;
    private Toolbar toolbar;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    int size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initialize();
        readRiverData();
        BackgroundTask task = new BackgroundTask(HomeActivity.this);
        task.execute();
    }

    private void initialize(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = findViewById(R.id.recycler_view_home);
        mAdapter = new HomeAdapter(timelineList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        bottomNavigationView = findViewById(R.id.navigationBottomBarHome);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.navigation_home){

                }
                if (id == R.id.navigation_geotagging){
                    goToMapsActivity();
                }
                if (id == R.id.navigation_your_timeline){
                    goToUserTimelineActivity();
                }
                return true;
            }
        });
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                timeline = timelineList.get(position);
                Log.d("okok", timeline.toString());
                likeIcon = view.findViewById(R.id.likeIconHome);
                imageView = view.findViewById(R.id.commentIconHome);
                textView = view.findViewById(R.id.commentCountHome);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goToCommentActivity(timeline.getUserID(), timeline.getDatabaseRef());
                    }
                });
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goToCommentActivity(timeline.getUserID(), timeline.getDatabaseRef());
                    }
                });
                likeIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (likeIcon.isChecked()){
                            if (!timeline.getArrayListLike().contains(getUserID())){
                                ArrayList<String> arrayList = timeline.getArrayListLike();
                                if (!arrayList.contains(getUserID())){
                                    arrayList.add(getUserID());
                                }
                                DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(timeline.getDatabaseRef());
                                editLike(ref, arrayList);
                                likeIcon.setEnabled(false);
                            }
                        }
                        if (!likeIcon.isChecked()){
                            ArrayList<String> arrayList = timeline.getArrayListLike();
                            if (arrayList.contains(getUserID())){
                                arrayList.remove(getUserID());
                            }
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(timeline.getDatabaseRef());
                            editLike(ref, arrayList);
                            likeIcon.setEnabled(false);
                        }
                    }
                });

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.profile_menu) {
            goToProfileActivity();
        }
        if  (id == R.id.leaderboard){
            goToLeaderboardActivity();
        }
        if(id == R.id.logout_menu){
            logout();
            System.out.println("logout");
        }
        if (id == R.id.toolbarActionBack){
            System.out.println("toolbar clicked");
        }
        return super.onOptionsItemSelected(item);
    }
    private void goToProfileActivity(){
        Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
        startActivity(intent);
    }
    private void goToLeaderboardActivity(){
        Intent intent = new Intent(getApplicationContext(), LeaderboardActivity.class);
        startActivity(intent);
    }
    public void logout(){
        mAuth.signOut();
        startActivity(new Intent(this, LoginActivity.class));
    }
    private void editLike(final DatabaseReference databaseReference, final ArrayList<String> arrayList){
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                databaseReference.child("arrayListLike").setValue(arrayList);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private String getUserID(){
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        String userID = currentFirebaseUser.getUid();
        return userID;
    }
    private void readRiverData(){
        DatabaseReference mRiver = FirebaseDatabase.getInstance().getReference("river");
        mRiver.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!timelineList.isEmpty()){
                    timelineList.clear();
                }
                size = (int)dataSnapshot.getChildrenCount();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River river = snapshot.getValue(River.class);
                    DatabaseReference databaseRefTimeline = snapshot.getRef();
                    Timeline timeline = new Timeline(river.getKondisiSungai(), river.getArrayListRiverActivity(), river.getRiverDescription(),
                            river.getUserID(), river.getUuid(), river.getRiverLocation(), river.getArrayListLike(), river.getUuidUserPhoto(),
                            river.getUsername(), databaseRefTimeline.toString(), river.getTimeStamp());
                    timelineList.add(timeline);
                }
                Collections.reverse(timelineList);
                mAdapter.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private void goToMapsActivity(){
        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
        startActivity(intent);
    }
    private void goToUserTimelineActivity(){
        Intent intent = new Intent(getApplicationContext(), TimelineUserActivity.class);
        startActivity(intent);
    }
    private void goToCommentActivity(String userID, String databaseReferenceSelectedRiver){
        Intent intent = new Intent(getApplicationContext(), CommentActivity.class);
        intent.putExtra("userID", userID);
        intent.putExtra("databaseReferenceSelectedRiver", databaseReferenceSelectedRiver);
        startActivity(intent);
    }

    private class BackgroundTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog;

        public BackgroundTask(HomeActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Retrieving Data, please wait...");
            dialog.show();
        }
        @Override
        protected void onPostExecute(Void result) {
            if (dialog.isShowing()) {
                mAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        }
        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
