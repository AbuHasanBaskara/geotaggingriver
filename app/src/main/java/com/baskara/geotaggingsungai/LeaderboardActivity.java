package com.baskara.geotaggingsungai;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class LeaderboardActivity extends AppCompatActivity {
    private List<User> leaderBoardList = new ArrayList<>();
    private RecyclerView recyclerView;
    private LeaderboardAdapter leaderboardAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);
        initialize();
        showLeaderboard();
    }
    private void initialize(){
        recyclerView = findViewById(R.id.recycler_view);
        leaderboardAdapter = new LeaderboardAdapter(leaderBoardList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(leaderboardAdapter);
    }
    private void showLeaderboard(){
        final DatabaseReference leaderBoard = FirebaseDatabase.getInstance().getReference("Users");
        Query mLeaderBoardHighest = leaderBoard.orderByChild("postCounter").limitToLast(10);
        mLeaderBoardHighest.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    User user = snapshot.getValue(User.class);
                    leaderBoardList.add(user);
                }
                Collections.reverse(leaderBoardList);
                leaderboardAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
}
