package com.baskara.geotaggingsungai;

import android.graphics.Movie;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class LeaderboardAdapter extends RecyclerView.Adapter<LeaderboardAdapter.MyViewHolder>{
    private List<User> leaderBoardList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewRank;
        public TextView textViewUsername;
        public TextView textViewAchievement;
        public TextView textViewPostCounter;
        public ImageView imageViewAchievementIcon;
        public MyViewHolder(View view) {
            super(view);
            textViewRank= view.findViewById(R.id.textViewRank);
            textViewUsername = view.findViewById(R.id.textViewUsername);
            textViewAchievement = view.findViewById(R.id.textViewAchivement);
            imageViewAchievementIcon = view.findViewById(R.id.imageViewAchievementIcon);
            textViewPostCounter = view.findViewById(R.id.textViewPostCounter);
        }
    }


    public LeaderboardAdapter(List<User> leaderBoardList) {
        this.leaderBoardList = leaderBoardList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leaderboard_list_row, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        User user = leaderBoardList.get(position);
        holder.textViewRank.setText("#"+String.valueOf(position+1));
        if (user.getCurrentAchievement().equalsIgnoreCase("Newbie")){
            holder.imageViewAchievementIcon.setImageResource(R.drawable.achievement_newbie);
        }
        else if (user.getCurrentAchievement().equalsIgnoreCase("Local Activist")){
            holder.imageViewAchievementIcon.setImageResource(R.drawable.achievement_local_activist);
        }
        else if (user.getCurrentAchievement().equalsIgnoreCase("River Activist")){
            holder.imageViewAchievementIcon.setImageResource(R.drawable.achievement_river_activist);
        }
        else if (user.getCurrentAchievement().equalsIgnoreCase("River Saver")){
            holder.imageViewAchievementIcon.setImageResource(R.drawable.achievement_river_saver);
        }
        else if (user.getCurrentAchievement().equalsIgnoreCase("Guardian of the River")){
            holder.imageViewAchievementIcon.setImageResource(R.drawable.achievement_guardian_of_the_river);
        }
        else if (user.getCurrentAchievement().equalsIgnoreCase("Top of Guardian")){
            holder.imageViewAchievementIcon.setImageResource(R.drawable.achievement_top_of_guardian);
        }
        else{
            holder.imageViewAchievementIcon.setImageResource(R.drawable.achievement_earth_saver);
        }
        holder.textViewUsername.setText(user.getUsername());
        holder.textViewAchievement.setText(user.getCurrentAchievement());
        if (user.getPostCounter()<2){
            holder.textViewPostCounter.setText(user.getPostCounter()+" Post");
        }
        else{
            holder.textViewPostCounter.setText(user.getPostCounter()+" Posts");
        }
    }

    @Override
    public int getItemCount() {
        return leaderBoardList.size();
    }
}
