package com.baskara.geotaggingsungai;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.heatmaps.Gradient;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener,
        AdapterView.OnItemSelectedListener, GoogleMap.OnMarkerClickListener {
    private static final String TAG = "MapsActivity";
    private static final String FINE_LOCATION = android.Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = android.Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;
    ArrayList<LatLng> listCleanRiver = new ArrayList<LatLng>();
    ArrayList<LatLng> listDirtyRiver = new ArrayList<LatLng>();
    private Boolean mLocationPermissionGranted = false;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private FloatingActionButton floatingActionButton;
    private Toolbar toolbar;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private BottomNavigationView bottomNavigationView;
    private double latitudeTemporary;
    private Spinner spinnerHeatmap;
    private Spinner spinnerMarker;
    private double longitudeTemporary;
    private TileOverlay mOverlay;
    private Marker mapMarker;
    String[] heatmap = {"Select....","semua sungai","sungai kotor","sungai bersih"};
    String[] marker = {"Select....","Cemar berat","Cemar sedang","Cemar ringan","Cukup baik","Sangat baik","memancing","berenang"};
    int[] colorsDirtyRiver = {
            Color.rgb(204, 102, 0), // coklat mudah
            Color.rgb(153, 76, 0)    // coklat tua
    };
    int[] colorsCleanRiver = {
            Color.rgb(51, 255, 51), // hijau muda
            Color.rgb(0, 255, 0)    // hijau
    };
    float[] startPoints = {
            0.2f, 1f
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        initialize();
        getLocationPermission();
        checkGPSActivated();
        BackgroundTask task = new BackgroundTask(MapsActivity.this);
        task.execute();
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(MapsActivity.this);
    }

    private void getLocationPermission() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(), FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(), COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionGranted = true;
                initMap();
            } else {
                ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST_CODE);
            }
        } else {
            ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "gettingPermissions");
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionGranted = false;
                            return;
                        }
                    }
                    mLocationPermissionGranted = true;
                    //inisialisasi map
                    initMap();
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(this, "Map is ready", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "Map is ready");
        mMap = googleMap;
        if (mLocationPermissionGranted) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                    (this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
        }
    }
    private void geotaggingRiver(){
        Log.d(TAG, "gettingDeviceCurrentLocation");
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        try {
            if (mLocationPermissionGranted){
                Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                       if (task.isSuccessful()){
                           Log.d(TAG,"onComplete : Found Location");
                           Location currentLocation = (Location) task.getResult();
                           goToRiverConditionActivity(currentLocation.getLatitude(), currentLocation.getLongitude(),
                                   getLocationName(currentLocation.getLatitude(), currentLocation.getLongitude()));
                       }else{
                           Log.d(TAG,"onComplete : Current Location is null");
                           Toast.makeText(MapsActivity.this, "Unable to get current location", Toast.LENGTH_SHORT).show();
                       }
                    }
                });
            }
        }catch (SecurityException e){
            Log.e(TAG, "gettingDeviceCurrentLocation : SecurityException"+ e.getMessage());
        }
    }
    private String getLocationName(double latitude, double longitude){
        String locationName = "unknown";
        try {
            Geocoder geo = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = geo.getFromLocation(latitude, longitude, 1);
            if (addresses.isEmpty()) {
                System.out.println("alamat kosong");
            }
            else {
                if (addresses.size() > 0) {
                    locationName = addresses.get(0).getLocality() + ", "
                            + addresses.get(0).getSubAdminArea() +", " + addresses.get(0).getAdminArea() + ", "
                            + addresses.get(0).getCountryName();
                }
            }
        }catch (Exception e){

        }
        return locationName;
    }
    private void getCurrentLocation(){
        Log.d(TAG, "gettingDeviceCurrentLocation");
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        try {
            if (mLocationPermissionGranted){
                Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()){
                            Location currentLocation = (Location) task.getResult();
                            moveCameraToCurrentLocation(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
                                    DEFAULT_ZOOM);
                        }else{
                            Log.d(TAG,"onComplete : Current Location is null");
                            Toast.makeText(MapsActivity.this, "Unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }catch (SecurityException e){
            Log.e(TAG, "gettingDeviceCurrentLocation : SecurityException"+ e.getMessage());
        }
    }
    private void moveCameraToCurrentLocation(LatLng latLng, float zoom){
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }
    private void checkGPSActivated(){
        final LocationManager manager = (LocationManager) getSystemService( this.LOCATION_SERVICE );
        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
    private ArrayList<LatLng> readItems(int resource) throws JSONException {
        ArrayList<LatLng> list = new ArrayList<LatLng>();
        InputStream inputStream = getResources().openRawResource(resource);
        String json = new Scanner(inputStream).useDelimiter("\\A").next();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);
            double lat = object.getDouble("lat");
            double lng = object.getDouble("lng");
            list.add(new LatLng(lat, lng));
        }
        return list;
    }
    private void initialize(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        floatingActionButton = findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(this);
        bottomNavigationView = findViewById(R.id.navigationBottomBar);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.navigation_home){
                    goToHomeActivity();
                }
                if (id == R.id.navigation_geotagging){

                }
                if (id == R.id.navigation_your_timeline){
                    goToTimelineUserActivity();
                }
                return true;
            }
        });
        spinnerHeatmap = findViewById(R.id.spinnerHeatmap);
        spinnerMarker = findViewById(R.id.spinnerMarker);
        ArrayAdapter<String>spinner_heatmap_adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, heatmap);
        ArrayAdapter<String>spinner_marker_adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, marker);
        spinner_heatmap_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_marker_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerHeatmap.setAdapter(spinner_heatmap_adapter);
        spinnerMarker.setAdapter(spinner_marker_adapter);
        spinnerHeatmap.setOnItemSelectedListener(this);
        spinnerMarker.setOnItemSelectedListener(this);
    }
    private void goToHomeActivity(){
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }
    private void goToTimelineUserActivity(){
        Intent intent = new Intent(getApplicationContext(), TimelineUserActivity.class);
        startActivity(intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.profile_menu) {
            goToProfileActivity();
        }
        if  (id == R.id.leaderboard){
            goToLeaderboardActivity();
        }
        if(id == R.id.logout_menu){
            logout();
            System.out.println("logout");
        }
        if (id == R.id.toolbarActionBack){
            System.out.println("toolbar clicked");
        }
        return super.onOptionsItemSelected(item);
    }

    private void goToRiverConditionActivity(double latitude, double longitude, String riverLocation){
        Intent intent = new Intent(getApplicationContext(), RiverConditionActivity.class);
        intent.putExtra("latitude",latitude);
        intent.putExtra("longitude",longitude);
        intent.putExtra("riverLocation", riverLocation);
        startActivity(intent);
    }
    private void goToDetailRiverActivity(double latitude, double longitude, String uuid, String userID){
        Intent intent = new Intent(getApplicationContext(), DetailRiverActivity.class);
        intent.putExtra("latitude",latitude);
        intent.putExtra("longitude",longitude);
        intent.putExtra("uuid", uuid);
        intent.putExtra("userID", userID);
        startActivity(intent);
    }
    private void goToProfileActivity(){
        Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
        startActivity(intent);
    }
    private void goToLeaderboardActivity(){
        Intent intent = new Intent(getApplicationContext(), LeaderboardActivity.class);
        startActivity(intent);
    }
    @Override
    public void onClick(View view) {
        if (view == floatingActionButton){
            geotaggingRiver();
        }
    }
    public void logout(){
        mAuth.signOut();
        startActivity(new Intent(this, LoginActivity.class));
    }
    public void showRiverActivity(final String reference){
        if (mOverlay!=null){
            mOverlay.remove();
            mMap.clear();
        }
        if (mapMarker!=null){
            mapMarker.remove();
            mMap.clear();
        }
        DatabaseReference mRiver = FirebaseDatabase.getInstance().getReference("river");
        Log.d(TAG,mRiver.toString());
        mRiver.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River river = snapshot.getValue(River.class);
                    ArrayList arrayList = river.getArrayListRiverActivity();
                    showRiverActivityByMarker(arrayList, reference, river.getLatitude(),
                            river.getLongitude(), river.getKondisiSungai(), river.getUuid(), river.getUserID());
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    public void showRiverCondition(final String reference, final String riverCondition){
        if (mOverlay!=null){
            mOverlay.remove();
            mMap.clear();
        }
        if (mapMarker!=null){
            mapMarker.remove();
            mMap.clear();
        }
        DatabaseReference dirtyRiver = FirebaseDatabase.getInstance().getReference(reference);
        Log.d(TAG,dirtyRiver.toString());
        dirtyRiver.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River river = snapshot.getValue(River.class);
                    if (river.getKondisiSungai().equalsIgnoreCase(riverCondition)){
                        showRiverConditionByMarker(river.getLatitude(), river.getLongitude(), river.getKondisiSungai()
                                , river.getUuid(), river.userID);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void showRiverConditionByMarker(double latitude, double longitude, String riverCondition, String uuid, String userID){
        mapMarker = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude)).
                        icon(getMarkerIcon(getColorMarkerBasedOnRiverCondition(riverCondition)))
                .title(uuid).snippet(userID));
        moveCameraToCurrentLocation(new LatLng(latitude, longitude),DEFAULT_ZOOM);
        mMap.setOnMarkerClickListener(this);

    }
    private void showRiverActivityByMarker(ArrayList arrayList, String reference, double latitude,
                                           double longitude, String riverCondition, String uuid, String userID){
        if (arrayList.contains(reference)){
            mapMarker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(latitude, longitude)).
                            icon(getMarkerIcon(getColorMarkerBasedOnRiverCondition(riverCondition)))
                    .title(uuid).snippet(userID));
            moveCameraToCurrentLocation(new LatLng(latitude, longitude),DEFAULT_ZOOM);
            mMap.setOnMarkerClickListener(this);
        }
    }
    @Override
    public boolean onMarkerClick(Marker marker) {
        Double latitude = marker.getPosition().latitude;
        Double longitude = marker.getPosition().longitude;
        String uuid = marker.getTitle();
        String userID = marker.getSnippet();
        goToDetailRiverActivity(latitude, longitude, uuid, userID);
        return false;
    }
    private String getColorMarkerBasedOnRiverCondition(String riverCondition){
        String colorCode="#FF0000";
        if (riverCondition.equalsIgnoreCase("Cemar berat")){
            colorCode = "#FF0000"; //red
        }
        if (riverCondition.equalsIgnoreCase("Cemar sedang")){
            colorCode = "#808080"; //gray
        }
        if (riverCondition.equalsIgnoreCase("Cemar ringan")){
            colorCode = "#8B4513"; //brown
        }
        if (riverCondition.equalsIgnoreCase("Cukup baik")){
            colorCode = "#FFFF00"; //yellow
        }
        if (riverCondition.equalsIgnoreCase("Sangat baik")){
            colorCode = "#00FF00"; //green
        }
        return colorCode;
    }
    private void clearMapOption(){
        if (mOverlay!=null){
            mOverlay.remove();
            mMap.clear();
        }
        if (mapMarker!=null){
            mapMarker.remove();
            mMap.clear();
        }
    }
    private void showHeatMapAllRiver(){
        clearMapOption();
        showHeatMapForDirtyRiver();
        showHeatMapForCleanRiver();
    }
    private void showHeatMapForDirtyRiver(){
        DatabaseReference river = FirebaseDatabase.getInstance().getReference("dirtyriver");
        Log.d(TAG,river.toString());
        river.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River mRiver = snapshot.getValue(River.class);
                    LatLng latLng = new LatLng(mRiver.getLatitude(),mRiver.getLongitude());
                    setSelectedListForHeatmap(latLng, "dirtyriver");
                    latitudeTemporary = mRiver.getLatitude();
                    longitudeTemporary = mRiver.getLongitude();
                }
                try{
                    showHeatMapWithTwoColors(getSelectedListForHeatmap("dirtyriver"),"dirtyriver");
                }catch (Exception e){
                    System.out.println("Error " + e.getMessage());
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void showHeatMapForCleanRiver(){
        DatabaseReference river = FirebaseDatabase.getInstance().getReference("cleanriver");
        Log.d(TAG,river.toString());
        river.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River mRiver = snapshot.getValue(River.class);
                    LatLng latLng = new LatLng(mRiver.getLatitude(),mRiver.getLongitude());
                    setSelectedListForHeatmap(latLng, "cleanriver");
                    latitudeTemporary = mRiver.getLatitude();
                    longitudeTemporary = mRiver.getLongitude();
                }
                try{
                    showHeatMapWithTwoColors(getSelectedListForHeatmap("cleanriver"),"cleanriver");
                }catch (Exception e){
                    System.out.println("Error " + e.getMessage());
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void showHeatMapWithTwoColors(ArrayList list, String reference){
        List<LatLng> listSungai = list;
        if (listSungai!=null){
            if (reference.equalsIgnoreCase("dirtyriver")){
                Gradient gradient = new Gradient(colorsDirtyRiver, startPoints);
                HeatmapTileProvider mProvider = new HeatmapTileProvider.Builder()
                        .data(listSungai)
                        .gradient(gradient)
                        .build();
                mOverlay = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
                moveCameraToCurrentLocation(new LatLng(latitudeTemporary, longitudeTemporary), DEFAULT_ZOOM);
            }
            if (reference.equalsIgnoreCase("cleanriver")){
                Gradient gradient = new Gradient(colorsCleanRiver, startPoints);
                HeatmapTileProvider mProvider = new HeatmapTileProvider.Builder()
                        .data(listSungai)
                        .gradient(gradient)
                        .build();
                mOverlay = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
                moveCameraToCurrentLocation(new LatLng(latitudeTemporary, longitudeTemporary), DEFAULT_ZOOM);
            }
        }
    }
    public void showHeatMap(final String reference){
        clearMapOption();
        DatabaseReference river = FirebaseDatabase.getInstance().getReference(reference);
        Log.d(TAG,river.toString());
        river.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River mRiver = snapshot.getValue(River.class);
                    LatLng latLng = new LatLng(mRiver.getLatitude(),mRiver.getLongitude());
                    setSelectedListForHeatmap(latLng, reference);
                    latitudeTemporary = mRiver.getLatitude();
                    longitudeTemporary = mRiver.getLongitude();
                }
                try{
                    showHeatMapFromList(getSelectedListForHeatmap(reference));
                }catch (Exception e){
                    System.out.println("Error " + e.getMessage());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void setSelectedListForHeatmap(LatLng latLng, String reference){
        if (reference.equalsIgnoreCase("dirtyriver")){
            listDirtyRiver.add(latLng);
        }
        if (reference.equalsIgnoreCase("cleanriver")){
            listCleanRiver.add(latLng);
        }
    }
    private ArrayList getSelectedListForHeatmap(String reference){
        ArrayList<LatLng> list = new ArrayList<LatLng>();
        if (reference.equalsIgnoreCase("dirtyriver")){
            list = listDirtyRiver;
        }
        if (reference.equalsIgnoreCase("cleanriver")){
            list = listCleanRiver;
        }
        return list;
    }
    private void showHeatMapFromList(ArrayList list){
        List<LatLng> listSungai = list;
        if (listSungai!=null){
            HeatmapTileProvider mProvider = new HeatmapTileProvider.Builder()
                    .data(listSungai)
                    .build();
            mOverlay = mMap.addTileOverlay(new TileOverlayOptions().tileProvider(mProvider));
            moveCameraToCurrentLocation(new LatLng(latitudeTemporary, longitudeTemporary), DEFAULT_ZOOM);
        }
    }
    public BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Spinner spinner1 = (Spinner)adapterView;
        Spinner spinner2 = (Spinner)adapterView;
        if (spinner1.getId() == R.id.spinnerHeatmap){
            String item = adapterView.getItemAtPosition(i).toString();
            if (item.equalsIgnoreCase("semua sungai")){
                showHeatMapAllRiver();
            }
            if (item.equalsIgnoreCase("sungai bersih")){
                showHeatMap("cleanriver");
            }
            if (item.equalsIgnoreCase("sungai kotor")){
                showHeatMap("dirtyriver");
            }
        }
        if (spinner2.getId() == R.id.spinnerMarker){
            String item = adapterView.getItemAtPosition(i).toString();
            if (item.equalsIgnoreCase("Cemar berat")){
                showRiverCondition("dirtyriver", "Cemar berat");
            }
            if (item.equalsIgnoreCase("Cemar sedang")){
                showRiverCondition("cleanriver", "Cemar sedang");
            }
            if (item.equalsIgnoreCase("Cemar ringan")){
                showRiverCondition("dirtyriver", "Cemar ringan");
            }
            if (item.equalsIgnoreCase("Cukup baik")){
                showRiverCondition("dirtyriver", "Cukup baik");
            }
            if (item.equalsIgnoreCase("Sangat baik")){
                showRiverCondition("cleanriver", "Sangat baik");
            }
            if (item.equalsIgnoreCase("pembersihan")){
                showRiverActivity("Pembersihan");
            }
            if (item.equalsIgnoreCase("memancing")){
                showRiverActivity("Memancing");
            }
            if (item.equalsIgnoreCase("berenang")){
                showRiverActivity("Berenang");
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private class BackgroundTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog;

        public BackgroundTask(MapsActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Loading Map, please wait...");
            dialog.show();
            showHeatMapAllRiver();
        }

        @Override
        protected void onPostExecute(Void result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
