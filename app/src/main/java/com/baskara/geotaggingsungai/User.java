package com.baskara.geotaggingsungai;

import java.util.ArrayList;

public class User {
    public String username;
    public int currentPoin;
    public int currentLevel;
    public String currentAchievement;
    public String uuid;
    public int postCounter;

    public User() {
    }

    public User(String username, int currentPoin, int currentLevel, String currentAchievement, String uuid, int postCounter) {
        this.username = username;
        this.currentPoin = currentPoin;
        this.currentLevel = currentLevel;
        this.currentAchievement = currentAchievement;
        this.uuid = uuid;
        this.postCounter = postCounter;
    }

    public int getPostCounter() {
        return postCounter;
    }

    public void setPostCounter(int postCounter) {
        this.postCounter = postCounter;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getCurrentPoin() {
        return currentPoin;
    }

    public void setCurrentPoin(int currentPoin) {
        this.currentPoin = currentPoin;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public String getCurrentAchievement() {
        return currentAchievement;
    }

    public void setCurrentAchievement(String currentAchievement) {
        this.currentAchievement = currentAchievement;
    }
}
