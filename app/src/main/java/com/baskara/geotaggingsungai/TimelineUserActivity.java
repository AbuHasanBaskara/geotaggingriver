package com.baskara.geotaggingsungai;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TimelineUserActivity extends AppCompatActivity {
    private BottomNavigationView bottomNavigationView;
    private List<Timeline> timelineList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TimelineUserAdapter mAdapter;
    private Timeline timeline;
    public String username;
    private CheckBox likeIcon;
    private ImageView imageView, editIcon, deleteIcon;
    private TextView textView;
    private Toolbar toolbar;
    private StorageReference mStorage = FirebaseStorage.getInstance().getReference();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
    int size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline__user_);
        initialize();
        readRiverData();
        BackgroundTask task = new BackgroundTask(TimelineUserActivity.this);
        task.execute();
    }

    private void initialize(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = findViewById(R.id.recycler_view_timeline_user);
        mAdapter = new TimelineUserAdapter(timelineList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        bottomNavigationView = findViewById(R.id.navigationBottomBarTimelineUser);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.navigation_home){
                    goToUserTimelineActivity();
                }
                if (id == R.id.navigation_geotagging){
                    goToMapsActivity();
                }
                if (id == R.id.navigation_your_timeline){

                }
                return true;
            }
        });
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                timeline = timelineList.get(position);
                Log.d("okok", timeline.toString());
                likeIcon = view.findViewById(R.id.likeIconTimelineUser);
                imageView = view.findViewById(R.id.commentIconTimelineUser);
                editIcon = view.findViewById(R.id.editIconTimelineUser);
                deleteIcon = view.findViewById(R.id.deleteIconTimelineUser);
                textView = view.findViewById(R.id.commentCountTimelineUser);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goToCommentActivity(timeline.getUserID(), timeline.getDatabaseRef());
                    }
                });
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goToCommentActivity(timeline.getUserID(), timeline.getDatabaseRef());
                    }
                });
                likeIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (likeIcon.isChecked()){
                            if (!timeline.getArrayListLike().contains(getUserID())){
                                ArrayList<String> arrayList = timeline.getArrayListLike();
                                if (!arrayList.contains(getUserID())){
                                    arrayList.add(getUserID());
                                }
                                DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(timeline.getDatabaseRef());
                                editLike(ref, arrayList);
                                likeIcon.setEnabled(false);
                            }
                        }
                        if (!likeIcon.isChecked()){
                            ArrayList<String> arrayList = timeline.getArrayListLike();
                            if (arrayList.contains(getUserID())){
                                arrayList.remove(getUserID());
                            }
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(timeline.getDatabaseRef());
                            editLike(ref, arrayList);
                            likeIcon.setEnabled(false);
                        }
                    }
                });
                editIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editRiverData(timeline);
                    }
                });
                deleteIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deleteRiverData(timeline);
                    }
                });
            }
            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
    private void editRiverData(final Timeline timeline){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Edit Post");
        alert.setMessage(timeline.getRiverDescription());
        final EditText input = new EditText(this);
        alert.setView(input);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String text = input.getText().toString();
                editDescriptionRiverData(timeline.getUuid(), text);
                editCleanRiverData(timeline.getUuid(), text);
                editDirtyRiverData(timeline.getUuid(), text);
                restartActivity("Post has been edited");
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });
        alert.show();
    }
    private void editCleanRiverData(final String uuid, final String input){
        DatabaseReference river = FirebaseDatabase.getInstance().getReference("cleanriver");
        river.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River mRiver = snapshot.getValue(River.class);
                    if (mRiver.getUuid().equalsIgnoreCase(uuid)){
                        DatabaseReference databaseReference = snapshot.getRef();
                        databaseReference.child("riverDescription").setValue(input);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void editDescriptionRiverData(final String uuid, final String input){
        DatabaseReference river = FirebaseDatabase.getInstance().getReference("river");
        river.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River mRiver = snapshot.getValue(River.class);
                    if (mRiver.getUuid().equalsIgnoreCase(uuid)){
                        DatabaseReference databaseReference = snapshot.getRef();
                        databaseReference.child("riverDescription").setValue(input);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void restartActivity(String notification){
        Toast.makeText(getApplicationContext(), notification,
                Toast.LENGTH_SHORT).show();
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
    private void editDirtyRiverData(final String uuid, final String input){
        DatabaseReference river = FirebaseDatabase.getInstance().getReference("dirtyriver");
        river.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River mRiver = snapshot.getValue(River.class);
                    if (mRiver.getUuid().equalsIgnoreCase(uuid)){
                        DatabaseReference databaseReference = snapshot.getRef();
                        databaseReference.child("riverDescription").setValue(input);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void deleteRiverData(final Timeline timeline){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure want to delete?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        deleteDirtyRiverData(timeline.getUuid());
                        deleteCleanRiverData(timeline.getUuid());
                        deleteRiverDataInRiver(timeline.getUuid());
                        deleteCurrentRiverPhoto(timeline.getUuid());
                        finish();
                        restartActivity("Post has been deleted");
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
    private void deleteCleanRiverData(final String uuid){
        DatabaseReference river = FirebaseDatabase.getInstance().getReference("cleanriver");
        river.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River mRiver = snapshot.getValue(River.class);
                    if (mRiver.getUuid().equalsIgnoreCase(uuid)){
                        DatabaseReference databaseReference = snapshot.getRef();
                        databaseReference.setValue(null).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        });
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void deleteRiverDataInRiver(final String uuid){
        DatabaseReference river = FirebaseDatabase.getInstance().getReference("river");
        river.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River mRiver = snapshot.getValue(River.class);
                    if (mRiver.getUuid().equalsIgnoreCase(uuid)){
                        DatabaseReference databaseReference = snapshot.getRef();
                        databaseReference.setValue(null).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        });
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void deleteDirtyRiverData(final String uuid){
        DatabaseReference river = FirebaseDatabase.getInstance().getReference("dirtyriver");
        river.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River mRiver = snapshot.getValue(River.class);
                    if (mRiver.getUuid().equalsIgnoreCase(uuid)){
                        DatabaseReference databaseReference = snapshot.getRef();
                        databaseReference.setValue(null).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                            }
                        });
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.profile_menu) {
            goToProfileActivity();
        }
        if  (id == R.id.leaderboard){
            goToLeaderboardActivity();
        }
        if(id == R.id.logout_menu){
            logout();
            System.out.println("logout");
        }
        if (id == R.id.toolbarActionBack){
            System.out.println("toolbar clicked");
        }
        return super.onOptionsItemSelected(item);
    }
    private void goToProfileActivity(){
        Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
        startActivity(intent);
    }
    private void goToLeaderboardActivity(){
        Intent intent = new Intent(getApplicationContext(), LeaderboardActivity.class);
        startActivity(intent);
    }
    public void logout(){
        mAuth.signOut();
        startActivity(new Intent(this, LoginActivity.class));
    }
    private void editLike(final DatabaseReference databaseReference, final ArrayList<String> arrayList){
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                databaseReference.child("arrayListLike").setValue(arrayList);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private String getUserID(){
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        String userID = currentFirebaseUser.getUid();
        return userID;
    }
    private void readRiverData(){
        DatabaseReference mRiver = FirebaseDatabase.getInstance().getReference("river");
        mRiver.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!timelineList.isEmpty()){
                    timelineList.clear();
                }
                size = (int)dataSnapshot.getChildrenCount();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River river = snapshot.getValue(River.class);
                    if (currentFirebaseUser.getUid().equalsIgnoreCase(river.getUserID())){
                        DatabaseReference databaseRefTimeline = snapshot.getRef();
                        Timeline timeline = new Timeline(river.getKondisiSungai(), river.getArrayListRiverActivity(), river.getRiverDescription(),
                                river.getUserID(), river.getUuid(), river.getRiverLocation(), river.getArrayListLike(), river.getUuidUserPhoto(),
                                river.getUsername(), databaseRefTimeline.toString(), river.getTimeStamp());
                        timelineList.add(timeline);
                    }
                }
                Collections.reverse(timelineList);
                mAdapter.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private void goToMapsActivity(){
        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
        startActivity(intent);
    }
    private void goToUserTimelineActivity(){
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }
    private void goToCommentActivity(String userID, String databaseReferenceSelectedRiver){
        Intent intent = new Intent(getApplicationContext(), CommentActivity.class);
        intent.putExtra("userID", userID);
        intent.putExtra("databaseReferenceSelectedRiver", databaseReferenceSelectedRiver);
        startActivity(intent);
    }
    private void deleteCurrentRiverPhoto(String currentUuid){
        StorageReference photoRef = mStorage.child(currentUuid);
        photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                System.out.println("deleted");
            }
        });
    }
    private class BackgroundTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog;

        public BackgroundTask(TimelineUserActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Retrieving Data, please wait...");
            dialog.show();
        }
        @Override
        protected void onPostExecute(Void result) {
            if (dialog.isShowing()) {
                mAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        }
        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
