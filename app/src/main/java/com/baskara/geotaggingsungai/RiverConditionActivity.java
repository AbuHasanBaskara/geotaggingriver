package com.baskara.geotaggingsungai;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

public class RiverConditionActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener{
    private static final String TAG = "RiverConditionActivity";
    private Toolbar toolbar_other;
    private CheckBox checkBox1Sampah;
    private CheckBox checkBox2Limbah;
    private CheckBox checkBox3Minyak;
    private CheckBox checkBox4Sabun;
    private CheckBox checkBox5Bau;
    private Button buttonNext;
    private int counter;
    private Spinner spinnerAliranSungai;
    private Spinner spinnerWarnaSungai;
    private boolean sampah = false;
    private boolean limbah = false;
    private boolean minyak = false;
    private boolean sabun = false;
    private boolean bau = false;
    private int poinAliranSungai;
    private int poinWarnaSungai;
    String[] aliranSungai = {"Select........", "Relatif deras", "Relatif tidak deras / tersumbat"};
    String[] warnaSungai = {"Select........", "Coklat", "Jernih", "Pekat"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_river_condition);
        initialize();
    }
    private void initialize(){
        checkBox1Sampah = findViewById(R.id.checkBox1Sampah);
        checkBox2Limbah = findViewById(R.id.checkBox2Limbah);
        checkBox3Minyak = findViewById(R.id.checkBox3Minyak);
        checkBox4Sabun = findViewById(R.id.checkBox4Sabun);
        checkBox5Bau = findViewById(R.id.checkBox5Bau);
        buttonNext = findViewById(R.id.buttonNext);
        checkBox1Sampah.setOnClickListener(this);
        checkBox2Limbah.setOnClickListener(this);
        checkBox3Minyak.setOnClickListener(this);
        checkBox4Sabun.setOnClickListener(this);
        checkBox5Bau.setOnClickListener(this);
        buttonNext.setOnClickListener(this);
        toolbar_other = findViewById(R.id.toolbar_other);
        setSupportActionBar(toolbar_other);
        spinnerAliranSungai = findViewById(R.id.spinnerAliranSungai);
        spinnerWarnaSungai = findViewById(R.id.spinnerWarnaAir);
        ArrayAdapter<String>spinner_aliran_sungai_adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, aliranSungai);
        ArrayAdapter<String>spinner_warna_sungai = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, warnaSungai);
        spinner_aliran_sungai_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_warna_sungai.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAliranSungai.setAdapter(spinner_aliran_sungai_adapter);
        spinnerWarnaSungai.setAdapter(spinner_warna_sungai);
        spinnerAliranSungai.setOnItemSelectedListener(this);
        spinnerWarnaSungai.setOnItemSelectedListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.checkBox1Sampah:
                if (checkBox1Sampah.isChecked()){
                    if (sampah==false){
                        sampah = true;
                        counter+=3;
                    }
                }
                else{
                    if (sampah==true){
                        sampah = false;
                        counter-=3;
                    }
                }
            case R.id.checkBox2Limbah:
                if (checkBox2Limbah.isChecked()){
                    if (limbah==false){
                        limbah = true;
                        counter+=3;
                    }
                }
                else{
                    if (limbah==true){
                        limbah = false;
                        counter-=3;
                    }
                }
            case R.id.checkBox3Minyak:
                if (checkBox3Minyak.isChecked()){
                    if (minyak==false){
                        minyak = true;
                        counter+=3;
                    }
                }
                else{
                    if (minyak==true){
                        minyak = false;
                        counter-=3;
                    }
                }
            case R.id.checkBox4Sabun:
                if (checkBox4Sabun.isChecked()){
                    if (sabun==false){
                        sabun = true;
                        counter+=3;
                    }
                }
                else{
                    if (sabun==true){
                        sabun = false;
                        counter-=3;
                    }
                }
            case R.id.checkBox5Bau:
                if (checkBox5Bau.isChecked()){
                    if (bau==false){
                        bau = true;
                        counter+=3;
                    }
                }
                else{
                    if (bau==true){
                        bau = false;
                        counter-=3;
                    }
                }
        }
        if (view == buttonNext){
            if (poinWarnaSungai>0&&poinAliranSungai>0){
                riverClassification();
            }else{
                Toast.makeText(this, "Please fill the form first", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void riverClassification(){
        if (counter+poinAliranSungai+poinWarnaSungai>=18){
            goToFormGeotaggingActivity("Cemar berat");
        }
        else if (counter+poinAliranSungai+poinWarnaSungai>=12){
            goToFormGeotaggingActivity("Cemar sedang");
        }
        else if (counter+poinAliranSungai+poinWarnaSungai>=9){
            goToFormGeotaggingActivity("Cemar ringan");
        }
        else if (counter+poinAliranSungai+poinWarnaSungai>=3){
            goToFormGeotaggingActivity("Cukup baik");
        }
        else if (counter+poinAliranSungai+poinWarnaSungai==2){
            goToFormGeotaggingActivity("Sangat baik");
        }
    }
    private double getLatitude(){
        Bundle extras = getIntent().getExtras();
        double latitude = 0;
        if (extras!=null){
            latitude = extras.getDouble("latitude");
            Log.d(TAG, "get latitude"+extras.getDouble("latitude"));
        }
        return latitude;
    }
    private double getLongitude(){
        Bundle extras = getIntent().getExtras();
        double longitude = 0;
        if (extras!=null){
            longitude = extras.getDouble("longitude");
            Log.d(TAG, "get longitude"+extras.getDouble("longitude"));
        }
        return longitude;
    }
    private String getRiverLocation(){
        Bundle extras = getIntent().getExtras();
        String riverLocation = "";
        if (extras!=null){
            riverLocation = extras.getString("riverLocation");
        }
        return riverLocation;
    }
    private void goToFormGeotaggingActivity(String kondisiSungai){
        Intent intent = new Intent(getApplicationContext(), FormGeotaggingActivity.class);
        intent.putExtra("latitude",getLatitude());
        intent.putExtra("longitude",getLongitude());
        intent.putExtra("kondisiSungai", kondisiSungai);
        intent.putExtra("riverLocation", getRiverLocation());
        startActivity(intent);
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        Spinner spinnerAliran = (Spinner)adapterView;
        Spinner spinnerWarna = (Spinner)adapterView;
        if (spinnerAliran.getId() == R.id.spinnerAliranSungai){
            String item = adapterView.getItemAtPosition(position).toString();
            if (item.equalsIgnoreCase("Relatif deras")){
                poinAliranSungai=1;
            }
            if (item.equalsIgnoreCase("Relatif tidak deras / tersumbat")){
                poinAliranSungai=3;
            }
            if (item.equalsIgnoreCase("Select........")){
                poinAliranSungai=0;
            }
        }
        if (spinnerWarna.getId() == R.id.spinnerWarnaAir){
            String item = adapterView.getItemAtPosition(position).toString();
            if (item.equalsIgnoreCase("Coklat")){
                poinWarnaSungai=2;
            }
            if (item.equalsIgnoreCase("Jernih")){
                poinWarnaSungai=1;
            }
            if (item.equalsIgnoreCase("Pekat")){
                poinWarnaSungai=3;
            }
            if (item.equalsIgnoreCase("Select........")){
                poinWarnaSungai=0;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
