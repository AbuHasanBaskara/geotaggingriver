package com.baskara.geotaggingsungai;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{
    private Button buttonRegister;
    private EditText editTextUsernameRegister;
    private EditText editTextEmailRegister;
    private EditText editTextPasswordRegister;
    private TextView textViewGoToLogin;
    private TextInputLayout input_layout_email_register;
    private TextInputLayout input_layout_username_register;
    private TextInputLayout input_layout_password_register;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase =  FirebaseDatabase.getInstance().getReference();
    private int defaultPoin = 0;
    private int defaultLevel = 1;
    private String defaultImageID = "default";
    private String defaultAchievement = "Newbie";
    private ArrayList<String> arrayListPost = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initialize();
    }
    private void initialize(){
        buttonRegister = findViewById(R.id.buttonRegister);
        editTextEmailRegister = findViewById(R.id.editTextEmailRegister);
        editTextPasswordRegister = findViewById(R.id.editTextPasswordRegister);
        editTextUsernameRegister = findViewById(R.id.editTextUsernameRegister);
        editTextEmailRegister.addTextChangedListener(new MyTextWatcher(editTextEmailRegister));
        editTextPasswordRegister.addTextChangedListener(new MyTextWatcher(editTextPasswordRegister));
        editTextUsernameRegister.addTextChangedListener(new MyTextWatcher(editTextUsernameRegister));
        input_layout_email_register = findViewById(R.id.input_layout_email_register);
        input_layout_password_register = findViewById(R.id.input_layout_password_register);
        input_layout_username_register = findViewById(R.id.input_layout_username_register);
        textViewGoToLogin = findViewById(R.id.textViewGoToLogin);
        SpannableString underlined = new SpannableString("Do not have account? Login here");
        underlined.setSpan(new UnderlineSpan(),0,underlined.length(),0);
        textViewGoToLogin.setText(underlined);
        textViewGoToLogin.setOnClickListener(this);
        buttonRegister.setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onClick(View view) {
        if (view == textViewGoToLogin){
            goToLoginActivity();
        }
        if (view == buttonRegister){
            submitForm();
        }
    }
    private boolean validatePassword() {
        if (editTextPasswordRegister.getText().toString().trim().isEmpty()) {
            input_layout_password_register.setError(getString(R.string.err_msg_password));
            requestFocus(editTextPasswordRegister);
            return false;
        }
        else if (editTextPasswordRegister.getText().length()<6){
            input_layout_password_register.setError(getString(R.string.err_msg_password_character_minimum));
            requestFocus(editTextPasswordRegister);
            return false;
        }
        else {
            input_layout_password_register.setErrorEnabled(false);
        }

        return true;
    }
    private boolean validateUsername() {
        if (editTextUsernameRegister.getText().toString().trim().isEmpty()) {
            input_layout_username_register.setError(getString(R.string.err_msg_name));
            requestFocus(editTextUsernameRegister);
            return false;
        }
        else {
            input_layout_username_register.setErrorEnabled(false);
        }
        return true;
    }
    private boolean validateEmail() {
        String email = editTextEmailRegister.getText().toString().trim();
        if (email.isEmpty() || !isValidEmail(email)) {
            input_layout_email_register.setError(getString(R.string.err_msg_email));
            requestFocus(editTextEmailRegister);
            return false;
        } else {
            input_layout_email_register.setErrorEnabled(false);
        }

        return true;
    }
    private void submitForm() {
        if (!validateEmail()) {
            return;
        }
        if (!validateUsername()){
            return;
        }
        if (!validatePassword()) {
            return;
        }
        else{
            String email = editTextEmailRegister.getText().toString().trim();
            String password = editTextPasswordRegister.getText().toString().trim();
            createUserWithEmailAndPassword(email, password);
        }
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    private void goToLoginActivity(){
        startActivity(new Intent(this, LoginActivity.class));
    }
    private void goToHomeActivity(){
        startActivity(new Intent(this, HomeActivity.class));
    }
    private void createUserWithEmailAndPassword(String email, String password){
        final String TAG = "Logcat";
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser currentUser = mAuth.getCurrentUser();
                            createUserProfileData(currentUser);
                            updateUI(currentUser);
                        } else {

                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            goToHomeActivity();
        }
    }
    private void createUserProfileData(FirebaseUser currentUser){
        User user = new User(editTextUsernameRegister.getText().toString(), defaultPoin, defaultLevel,
                defaultAchievement, defaultImageID, 0);
        mDatabase.child("Users").child(currentUser.getUid()).setValue(user);
        Log.d("createUserProfileData", "users success");
    }
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.editTextEmailRegister:
                    validateEmail();
                    break;
                case R.id.editTextPasswordRegister:
                    validatePassword();
                    break;
            }
        }
    }
}
