package com.baskara.geotaggingsungai;

public interface OnBottomReachedListener {
    void onBottomReached(int position);
}
