package com.baskara.geotaggingsungai;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.UUID;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener{
    private static final int REQUEST_WRITE_PERMISSION = 786;
    private ImageView profileEditPicture;
    private TextInputLayout input_layout_edit_username;
    private TextInputLayout input_layout_edit_old_password;
    private TextInputLayout input_layout_edit_new_password;
    ProgressDialog progressDialog;
    private FloatingActionButton floatingEditPhoto;
    FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private StorageReference mStorage = FirebaseStorage.getInstance().getReference();
    private StorageReference imageRef;
    private EditText editTextNewUsername;
    private EditText editTextOldPassword;
    private EditText editTextNewPassword;
    private String oldPassword;
    private UuidPhoto uuidPhoto;
    Bitmap mImageUri;
    private Toolbar toolbar_other;
    private Button buttonSubmitEditProfile;
    String newUuid = UUID.randomUUID().toString();
    private boolean imageStatusChanged = false;
    String currentUuid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initialize();
        readUserData();
        requestPermissionReadFromGallery();
    }
    private void initialize(){
        profileEditPicture = findViewById(R.id.profileEditPicture);
        editTextNewPassword = findViewById(R.id.editTextNewPassword);
        editTextOldPassword = findViewById(R.id.editTextOldPassword);
        editTextNewUsername = findViewById(R.id.editTextNewUsername);
        buttonSubmitEditProfile = findViewById(R.id.buttonSubmitEditProfile);
        input_layout_edit_old_password = findViewById(R.id.input_layout_edit_old_password);
        input_layout_edit_username = findViewById(R.id.input_layout_edit_username);
        input_layout_edit_new_password = findViewById(R.id.input_layout_edit_new_password);
        floatingEditPhoto = findViewById(R.id.floatingActionEditPhoto);
        toolbar_other = findViewById(R.id.toolbar_other);
        setSupportActionBar(toolbar_other);
        floatingEditPhoto.setOnClickListener(this);
        buttonSubmitEditProfile.setOnClickListener(this);
    }
    private void readUserData(){
        DatabaseReference userDatabase = FirebaseDatabase.getInstance().getReference("Users");
        DatabaseReference userDatabaseByUserID = userDatabase.child(currentFirebaseUser.getUid());
        userDatabaseByUserID.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                editTextNewUsername.setText(user.getUsername());
                currentUuid = user.getUuid();
                showProfilePicture(currentUuid);
                uuidPhoto = new UuidPhoto(currentUuid, setGenerateName(newUuid));
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(EditProfileActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void showProfilePicture(String uuid){
        if (uuid.equalsIgnoreCase("default")){
            profileEditPicture.setImageResource(R.drawable.default_picture);
        }
        else{
            imageRef = mStorage.child(uuid);
            imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Uri url = uri;
                    Picasso.with(getApplicationContext()).load(url).into(profileEditPicture);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });
        }
    }
    @Override
    public void onClick(View view) {
        if (view == floatingEditPhoto){
            pickPhoto();
        }
        if (view == buttonSubmitEditProfile){
            submitForm();
        }
    }
    private void requestPermissionReadFromGallery() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        }
    }
    private void pickPhoto(){
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , 1);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (requestCode == 1){
            if(resultCode == RESULT_OK){
                final Uri uri = imageReturnedIntent.getData();
                try {
                    useImage(uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    void useImage(Uri uri) throws IOException {
        mImageUri = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
        profileEditPicture.setImageBitmap(mImageUri);
        imageStatusChanged = true;
    }
    private void updateUsername(){
        updateUsernameInUser();
        updateUsernameInRiverPost("river");
        updateUsernameInRiverPost("dirtyriver");
        updateUsernameInRiverPost("cleanriver");
        Toast.makeText(EditProfileActivity.this, "Username is changed", Toast.LENGTH_SHORT).show();
    }
    private void updateUsernameInUser(){
        DatabaseReference userDatabase = FirebaseDatabase.getInstance().getReference("Users");
        DatabaseReference userDatabaseByUserID = userDatabase.child(currentFirebaseUser.getUid());
        userDatabaseByUserID.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dataSnapshot.getRef().child("username").setValue(editTextNewUsername.getText().toString());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(EditProfileActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void updateUsernameInRiverPost(String ref){
        DatabaseReference mRiver = FirebaseDatabase.getInstance().getReference(ref);
        mRiver.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River river = snapshot.getValue(River.class);
                    if (currentFirebaseUser.getUid().equalsIgnoreCase(river.getUserID())){
                        DatabaseReference databaseReference = snapshot.getRef();
                        databaseReference.child("username").setValue(editTextNewUsername.getText().toString());
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void updateUuid(){
        DatabaseReference userDatabase = FirebaseDatabase.getInstance().getReference("Users");
        DatabaseReference userDatabaseByUserID = userDatabase.child(currentFirebaseUser.getUid());
        userDatabaseByUserID.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (!user.getUuid().equalsIgnoreCase("default")){
                    deleteCurrentPhotoProfile();
                }
                dataSnapshot.getRef().child("uuid").setValue(newUuid);
                saveProfilePhotoToFirebaseStorage();
                Toast.makeText(EditProfileActivity.this, "Success Edit Profile", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(EditProfileActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void submitForm(){
        if (editTextNewUsername.getText()!=null){
            updateUsername();
        }
        if (imageStatusChanged==true){
            updateUuid();
            backGroundTask();
        }
        if (editTextOldPassword.getText().length() > 0 && editTextNewPassword.length() > 0){
            String newPassword = editTextNewPassword.getText().toString();
            currentFirebaseUser.updatePassword(newPassword).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Password Wrong",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
            backGroundTask();
        }
    }
    private void backGroundTask(){
        BackgroundTask task = new BackgroundTask(EditProfileActivity.this);
        task.execute();
    }
    private String setGenerateName(final String newUuid){
        StorageReference cekname = mStorage.child(newUuid);
        if (!cekname.getName().equals(newUuid)){
            String new_uuid = UUID.randomUUID().toString();
            setGenerateName(new_uuid);
        }
        return newUuid;
    }
    private void deleteCurrentPhotoProfile(){
        StorageReference photoRef = mStorage.child(currentUuid);
        photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                System.out.println("deleted");
            }
        });
    }
    private void editDirtyRiverData(final String currentUuid, final String newUuid){
        DatabaseReference river = FirebaseDatabase.getInstance().getReference("dirtyriver");
        river.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River mRiver = snapshot.getValue(River.class);
                    if (mRiver.getUuidUserPhoto().equalsIgnoreCase(currentUuid)){
                        DatabaseReference databaseReference = snapshot.getRef();
                        databaseReference.child("uuidUserPhoto").setValue(newUuid);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void editRiverData(final String currentUuid, final String newUuid){
        DatabaseReference river = FirebaseDatabase.getInstance().getReference("river");
        river.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River mRiver = snapshot.getValue(River.class);
                    if (mRiver.getUuidUserPhoto().equalsIgnoreCase(currentUuid)){
                        DatabaseReference databaseReference = snapshot.getRef();
                        databaseReference.child("uuidUserPhoto").setValue(newUuid);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void editCleanRiverData(final String currentUuid, final String newUuid){
        DatabaseReference river = FirebaseDatabase.getInstance().getReference("cleanriver");
        river.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    River mRiver = snapshot.getValue(River.class);
                    if (mRiver.getUuidUserPhoto().equalsIgnoreCase(currentUuid)){
                        DatabaseReference databaseReference = snapshot.getRef();
                        databaseReference.child("uuidUserPhoto").setValue(newUuid);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void saveProfilePhotoToFirebaseStorage(){
        StorageReference uploadFileRef = mStorage.child(newUuid);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        mImageUri.compress(Bitmap.CompressFormat.JPEG, 70, baos);
        //data upload to firebase
        byte[] data2 = baos.toByteArray();

        UploadTask uploadTask = uploadFileRef.putBytes(data2);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                System.out.println("deleted and success");
            }
        });
    }
    private void goToProfileActivity(){
        startActivity(new Intent(this, ProfileActivity.class));
    }
    private class BackgroundTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog;

        public BackgroundTask(EditProfileActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Saving Data, please wait...");
            dialog.show();
            editRiverData(uuidPhoto.getCurrentUuid(), uuidPhoto.getNewUuid());
            editDirtyRiverData(uuidPhoto.getCurrentUuid(), uuidPhoto.getNewUuid());
            editCleanRiverData(uuidPhoto.getCurrentUuid(), uuidPhoto.getNewUuid());
        }

        @Override
        protected void onPostExecute(Void result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
                finish();
                goToProfileActivity();
            }
        }
        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}

