package com.baskara.geotaggingsungai;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentActivity extends AppCompatActivity implements View.OnClickListener{
    private StorageReference mStorage = FirebaseStorage.getInstance().getReference();
    private StorageReference imageRef;
    private List<Comment> commentList = new ArrayList<>();
    private Comment commentClass = new Comment();
    private RecyclerView recycler_view_comment;
    private CommentAdapter mAdapter;
    private EditText editTextInputComment;
    private CircleImageView circleImageViewProfileForComment;
    FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private ImageView imageViewSendComment;
    private String username;
    private String usernameCurrentUser;
    private String uuidCurrentUser;
    Map<String, String> commentMap=new HashMap<String, String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        initialize();
        readUserData();
        readPostData();
    }
    private void initialize(){
        recycler_view_comment = findViewById(R.id.recycler_view_comment);
        editTextInputComment = findViewById(R.id.editTextInputComment);
        circleImageViewProfileForComment = findViewById(R.id.circleImageViewProfileForComment);
        imageViewSendComment = findViewById(R.id.imageViewSendComment);
        mAdapter = new CommentAdapter(commentList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view_comment.setLayoutManager(mLayoutManager);
        recycler_view_comment.setItemAnimator(new DefaultItemAnimator());
        recycler_view_comment.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));
        recycler_view_comment.setAdapter(mAdapter);
        imageViewSendComment.setOnClickListener(this);
    }

    private void readPostData(){
        final DatabaseReference mRef = getDatabaseRef();
        DatabaseReference comment = mRef.child("comment");
        comment.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!commentList.isEmpty()){
                    commentList.clear();
                }
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Comment commentClass = snapshot.getValue(Comment.class);
                    commentList.add(commentClass);
                }
                mAdapter.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
    private void readUserData(){
        DatabaseReference userDatabase = FirebaseDatabase.getInstance().getReference("Users");
        DatabaseReference userDatabaseByUserID = userDatabase.child(currentFirebaseUser.getUid());
        userDatabaseByUserID.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                showProfilePicture(user.getUuid());
                uuidCurrentUser = user.getUuid();
                usernameCurrentUser = user.getUsername();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(CommentActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void showProfilePicture(String uuid){
        if (uuid.equalsIgnoreCase("default")){
            circleImageViewProfileForComment.setImageResource(R.drawable.default_picture);
        }
        else{
            imageRef = mStorage.child(uuid);
            imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Uri url = uri;
                    Picasso.with(getApplicationContext()).load(url).into(circleImageViewProfileForComment);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });
        }
    }
    private DatabaseReference getDatabaseRef(){
        Bundle extras = getIntent().getExtras();
        String url = "";
        if (extras!=null){
            url = extras.getString("databaseReferenceSelectedRiver");
        }
        DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(url);
        return ref;
    }

    @Override
    public void onClick(View view) {
        if (view == imageViewSendComment){
            updateCommentData();
        }
    }
    private void updateCommentData(){
        final DatabaseReference mRef = getDatabaseRef();
        Comment comment = new Comment(usernameCurrentUser, uuidCurrentUser, editTextInputComment.getText().toString());
        mRef.child("comment").push().setValue(comment);
        editTextInputComment.clearAnimation();
    }
}
