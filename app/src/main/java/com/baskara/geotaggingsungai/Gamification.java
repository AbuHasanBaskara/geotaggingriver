package com.baskara.geotaggingsungai;

public class Gamification {
    private String achievement;
    private int poin;
    private int level;

    public String getAchievement(int currentLevel) {
        if (currentLevel >= 30){
            achievement = "Earth Saver";
        }
        else if (currentLevel >= 20){
            achievement = "Top of Guardian";
        }
        else if (currentLevel >= 15){
            achievement = "Guardian of the River";
        }
        else if (currentLevel >= 10){
            achievement = "River Saver";
        }
        else if (currentLevel >= 5){
            achievement = "River Activist";
        }
        else if (currentLevel >=3){
            achievement = "Local Activist";
        }
        else{
            achievement = "Newbie";
        }
        return achievement;
    }

    public int getPoin(int currentLevel) {
        if (currentLevel >= 11){
            poin = 5;
        }
        else if (currentLevel >= 6){
            poin = 10;
        }
        else if (currentLevel >= 4){
            poin = 20;
        }
        else if (currentLevel == 3){
            poin = 25;
        }
        else if (currentLevel == 2){
            poin = 50;
        }
        else{
            poin = 100;
        }
        return poin;
    }

    public int getLevel(int currentPoin) {
        if (currentPoin==100){
            level = 1;
        }
        else{
            level = 0;
        }
        return level;
    }
}
