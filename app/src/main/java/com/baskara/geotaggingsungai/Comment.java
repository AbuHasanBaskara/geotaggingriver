package com.baskara.geotaggingsungai;

public class Comment {
    public String username;
    public String uuid;
    public String comment;

    public Comment(String username, String uuid, String comment) {
        this.username = username;
        this.uuid = uuid;
        this.comment = comment;
    }
    public Comment(){

    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
