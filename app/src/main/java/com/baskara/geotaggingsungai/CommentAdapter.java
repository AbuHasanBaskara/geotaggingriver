package com.baskara.geotaggingsungai;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder>{
    private List<Comment> commentList;
    private StorageReference mStorage = FirebaseStorage.getInstance().getReference();
    private StorageReference imageRef;
    private Context context;
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Comment comment = commentList.get(position);
        holder.commentUsername.setText(comment.getUsername());
        holder.textViewComment.setText(comment.getComment());
        if (comment.getUuid()!=null){
            if (comment.getUuid().equalsIgnoreCase("default")){
                holder.circleImageViewProfileForComment.setImageResource(R.drawable.default_picture);
            }
            else{
                imageRef = mStorage.child(comment.getUuid());
                imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Uri url = uri;
                        Picasso.with(context).load(url).into(holder.circleImageViewProfileForComment);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView commentUsername, textViewComment;
        public CircleImageView circleImageViewProfileForComment;
        public MyViewHolder(View view) {
            super(view);
            commentUsername = view.findViewById(R.id.commentUsername);
            textViewComment = view.findViewById(R.id.textViewComment);
            circleImageViewProfileForComment = view.findViewById(R.id.circleImageViewProfileForComment);
        }
    }
    public CommentAdapter(List<Comment> commentList, Context context) {
        this.commentList = commentList;
        this.context = context;
    }
}
