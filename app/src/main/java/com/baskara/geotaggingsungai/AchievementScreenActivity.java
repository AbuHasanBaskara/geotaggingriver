package com.baskara.geotaggingsungai;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static com.baskara.geotaggingsungai.R.drawable.achievement_newbie;

public class AchievementScreenActivity extends AppCompatActivity {
    private ImageView achievementIcon;
    private TextView textViewAchivement;
    private TextView textViewCurrentPoin;
    private TextView textViewIncreasedPoin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achievement_screen);
        initialize();
        showAchievement();
        showTextView();
        int secondsDelayed = 5;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(AchievementScreenActivity.this, HomeActivity.class));
                finish();
            }
        }, secondsDelayed * 1000);
    }
    private void showTextView(){
        textViewAchivement.setText("Level "+getLevel()+" . "+getAchievement());
        textViewCurrentPoin.setText("Poin Bar "+getPoin()+"/"+"100");
        textViewIncreasedPoin.setText("Poin Increased! +"+getIncreasedPoin());
    }
    private void showAchievement(){
        if (getAchievement().equalsIgnoreCase("Newbie")){
            achievementIcon.setImageResource(R.drawable.achievement_newbie);
        }
        else if (getAchievement().equalsIgnoreCase("Local Activist")){
            achievementIcon.setImageResource(R.drawable.achievement_local_activist);
        }
        else if (getAchievement().equalsIgnoreCase("River Activist")){
            achievementIcon.setImageResource(R.drawable.achievement_river_activist);
        }
        else if (getAchievement().equalsIgnoreCase("River Saver")){
            achievementIcon.setImageResource(R.drawable.achievement_river_saver);
        }
        else if (getAchievement().equalsIgnoreCase("Guardian of the River")){
            achievementIcon.setImageResource(R.drawable.achievement_guardian_of_the_river);
        }
        else if (getAchievement().equalsIgnoreCase("Top of Guardian")){
            achievementIcon.setImageResource(R.drawable.achievement_top_of_guardian);
        }
        else{
            achievementIcon.setImageResource(R.drawable.achievement_earth_saver);
        }
    }
    private void initialize(){
        achievementIcon = findViewById(R.id.achievementIcon);
        textViewAchivement = findViewById(R.id.textViewAchivement);
        textViewCurrentPoin = findViewById(R.id.textViewCurrentPoin);
        textViewIncreasedPoin = findViewById(R.id.textViewIncreasedPoin);
    }
    private String getAchievement(){
        Bundle extras = getIntent().getExtras();
        String achievement="";
        if (extras!=null){
            achievement = extras.getString("achievement");
        }
        return achievement;
    }
    private int getPoin(){
        Bundle extras = getIntent().getExtras();
        int poin= 0;
        if (extras!=null){
            poin = extras.getInt("poin");
        }
        return poin;
    }
    private int getIncreasedPoin(){
        Bundle extras = getIntent().getExtras();
        int increasedPoin= 0;
        if (extras!=null){
            increasedPoin = extras.getInt("increasedPoin");
        }
        return increasedPoin;
    }
    private int getLevel(){
        Bundle extras = getIntent().getExtras();
        int level= 0;
        if (extras!=null){
            level = extras.getInt("level");
        }
        return level;
    }
}
