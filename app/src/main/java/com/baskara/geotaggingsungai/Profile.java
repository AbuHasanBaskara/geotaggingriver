package com.baskara.geotaggingsungai;

public class Profile {
    public String username;
    public String title;
    public String poin;
    public String email;
    public String uuidPhoto;
    public String achievement;

    public Profile(String username, String title, String poin, String email, String uuidPhoto, String achievement) {
        this.username = username;
        this.title = title;
        this.poin = poin;
        this.email = email;
        this.uuidPhoto = uuidPhoto;
        this.achievement = achievement;
    }

    public String getUsername() {
        return username;
    }

    public String getTitle() {
        return title;
    }

    public String getPoin() {
        return poin;
    }

    public String getEmail() {
        return email;
    }

    public String getUuidPhoto() {
        return uuidPhoto;
    }

    public String getAchievement() {
        return achievement;
    }
}
