package com.baskara.geotaggingsungai;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = "ProfileActivity";
    private ImageView achievementIcon;
    private TextView textViewExperience;
    private TextView textViewEmail;
    private TextView textViewTitle;
    private TextView textViewUsername;
    private ImageView profilePicture;
    private Toolbar toolbar_other;
    private FloatingActionButton floatingActionEditProfile;
    private StorageReference mStorage = FirebaseStorage.getInstance().getReference();
    private StorageReference imageRef;
    private Profile profile;
    FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initialize();
        readUserData();
        BackgroundTask task = new BackgroundTask(ProfileActivity.this);
        task.execute();
    }
    private void initialize(){
        achievementIcon = findViewById(R.id.achievementIcon);
        textViewEmail = findViewById(R.id.textViewEmail);
        textViewExperience = findViewById(R.id.textViewExperience);
        textViewTitle = findViewById(R.id.textViewTitle);
        textViewUsername = findViewById(R.id.textViewUsername);
        profilePicture = findViewById(R.id.profilePicture);
        floatingActionEditProfile = findViewById(R.id.floatingActionEditProfile);
        floatingActionEditProfile.setOnClickListener(this);
        toolbar_other = findViewById(R.id.toolbar_other);
        setSupportActionBar(toolbar_other);
    }
    private void readUserData(){
        DatabaseReference userDatabase = FirebaseDatabase.getInstance().getReference("Users");
        DatabaseReference userDatabaseByUserID = userDatabase.child(currentFirebaseUser.getUid());
        Log.d(TAG, currentFirebaseUser.getUid());
        userDatabaseByUserID.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                profile = new Profile(user.getUsername(), "Level "+user.getCurrentLevel()+" . "+user.getCurrentAchievement(),
                        "Poin "+user.getCurrentPoin()+"/100", currentFirebaseUser.getEmail(), user.getUuid(), user.getCurrentAchievement());
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(ProfileActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showProfilePicture(String uuid){
        if (uuid.equalsIgnoreCase("default")){
            profilePicture.setImageResource(R.drawable.default_picture);
        }
        else{
            imageRef = mStorage.child(uuid);
            imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Uri url = uri;
                    Picasso.with(getApplicationContext()).load(url).into(profilePicture);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            });
        }
    }
    private void showProfile(){
        textViewUsername.setText(profile.getUsername());
        textViewTitle.setText(profile.getTitle());
        textViewExperience.setText(profile.getPoin());
        textViewEmail.setText(profile.getEmail());
        showProfilePicture(profile.getUuidPhoto());
        showAchievement(profile.getAchievement());
    }

    private void showAchievement(String achievement){
        if (achievement.equalsIgnoreCase("Newbie")){
            achievementIcon.setImageResource(R.drawable.achievement_newbie);
        }
        else if (achievement.equalsIgnoreCase("Local Activist")){
            achievementIcon.setImageResource(R.drawable.achievement_local_activist);
        }
        else if (achievement.equalsIgnoreCase("River Activist")){
            achievementIcon.setImageResource(R.drawable.achievement_river_activist);
        }
        else if (achievement.equalsIgnoreCase("River Saver")){
            achievementIcon.setImageResource(R.drawable.achievement_river_saver);
        }
        else if (achievement.equalsIgnoreCase("Guardian of the River")){
            achievementIcon.setImageResource(R.drawable.achievement_guardian_of_the_river);
        }
        else if (achievement.equalsIgnoreCase("Top of Guardian")){
            achievementIcon.setImageResource(R.drawable.achievement_top_of_guardian);
        }
        else{
            achievementIcon.setImageResource(R.drawable.achievement_earth_saver);
        }
        achievementIcon.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        if (view == floatingActionEditProfile){
            startActivity(new Intent(this, EditProfileActivity.class));
            finish();
        }
    }
    class BackgroundTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog;

        public BackgroundTask(ProfileActivity activity) {
            dialog = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Retrieving Data, please wait...");
            dialog.show();
        }
        @Override
        protected void onPostExecute(Void result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
                showProfile();
            }
        }
        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
