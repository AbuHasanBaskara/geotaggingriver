package com.baskara.geotaggingsungai;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class River {
    public String kondisiSungai;
    public ArrayList<String> arrayListRiverActivity = new ArrayList<String>();
    public double latitude;
    public double longitude;
    public String riverDescription;
    public String userID;
    public String uuid;
    public String username;
    public String riverLocation;
    public Map<String, Object> timeStamp;
    public ArrayList<String> arrayListLike = new ArrayList<>();
    public String uuidUserPhoto;
    public River() {
        // default constructor required for calls to datasnapshot.getValue(User.class)
    }

    public River(String kondisiSungai, ArrayList<String> arrayListRiverActivity, double latitude,
                 double longitude, String riverDescription, String userID, String uuid, String riverLocation,
                 Map<String, Object> timeStamp,ArrayList<String>arrayListLike, String uuidUserPhoto, String username) {
        this.kondisiSungai = kondisiSungai;
        this.arrayListRiverActivity = arrayListRiverActivity;
        this.latitude = latitude;
        this.longitude = longitude;
        this.riverDescription = riverDescription;
        this.userID = userID;
        this.uuid = uuid;
        this.riverLocation = riverLocation;
        this.timeStamp = timeStamp;
        this.arrayListLike = arrayListLike;
        this.uuidUserPhoto = uuidUserPhoto;
        this.username = username;
    }

    @Override
    public String toString() {
        return "River{" +
                "kondisiSungai='" + kondisiSungai + '\'' +
                ", arrayListRiverActivity=" + arrayListRiverActivity +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", riverDescription='" + riverDescription + '\'' +
                ", userID='" + userID + '\'' +
                ", uuid='" + uuid + '\'' +
                ", username='" + username + '\'' +
                ", riverLocation='" + riverLocation + '\'' +
                ", timeStamp=" + timeStamp +
                ", arrayListLike=" + arrayListLike +
                ", uuidUserPhoto='" + uuidUserPhoto + '\'' +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUuidUserPhoto() {
        return uuidUserPhoto;
    }

    public void setUuidUserPhoto(String uuidUserPhoto) {
        this.uuidUserPhoto = uuidUserPhoto;
    }

    public River(ArrayList<String> arrayListRiverActivity){
        this.arrayListRiverActivity = arrayListRiverActivity;
    }

    public ArrayList<String> getArrayListLike() {
        return arrayListLike;
    }

    public void setArrayListLike(ArrayList<String> arrayListLike) {
        this.arrayListLike = arrayListLike;
    }

    public Map<String, Object> getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Map<String, Object> timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getRiverLocation() {
        return riverLocation;
    }

    public void setRiverLocation(String riverLocation) {
        this.riverLocation = riverLocation;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getKondisiSungai() {
        return kondisiSungai;
    }

    public void setKondisiSungai(String kondisiSungai) {
        this.kondisiSungai = kondisiSungai;
    }

    public ArrayList<String> getArrayListRiverActivity() {
        return arrayListRiverActivity;
    }

    public void setArrayListRiverActivity(ArrayList<String> arrayListRiverActivity) {
        this.arrayListRiverActivity = arrayListRiverActivity;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getRiverDescription() {
        return riverDescription;
    }

    public void setRiverDescription(String riverDescription) {
        this.riverDescription = riverDescription;
    }
}
