package com.baskara.geotaggingsungai;

import java.util.ArrayList;
import java.util.Map;

public class Timeline {
    public String kondisiSungai;
    public ArrayList<String> arrayListRiverActivity = new ArrayList<String>();
    public String riverDescription;
    public String userID;
    public String uuid;
    public String riverLocation;
    public String databaseRef;
    public Map<String, Object> timeStamp;

    @Override
    public String toString() {
        return "Timeline{" +
                "kondisiSungai='" + kondisiSungai + '\'' +
                ", arrayListRiverActivity=" + arrayListRiverActivity +
                ", riverDescription='" + riverDescription + '\'' +
                ", userID='" + userID + '\'' +
                ", uuid='" + uuid + '\'' +
                ", riverLocation='" + riverLocation + '\'' +
                ", arrayListLike=" + arrayListLike +
                ", uuidUserPhoto='" + uuidUserPhoto + '\'' +
                ", username='" + username + '\'' +
                '}';
    }

    public ArrayList<String> arrayListLike = new ArrayList<>();
    public String uuidUserPhoto;
    public String username;
    public Timeline(){

    }
    public Timeline(String kondisiSungai, ArrayList<String> arrayListRiverActivity, String riverDescription, String userID, String uuid, String riverLocation , ArrayList<String> arrayListLike, String uuidUserPhoto, String username, String databaseRef, Map<String, Object> timeStamp) {
        this.kondisiSungai = kondisiSungai;
        this.arrayListRiverActivity = arrayListRiverActivity;
        this.riverDescription = riverDescription;
        this.userID = userID;
        this.uuid = uuid;
        this.riverLocation = riverLocation;
        this.arrayListLike = arrayListLike;
        this.uuidUserPhoto = uuidUserPhoto;
        this.username = username;
        this.databaseRef = databaseRef;
        this.timeStamp = timeStamp;
    }

    public Map<String, Object> getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Map<String, Object> timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getDatabaseRef() {
        return databaseRef;
    }

    public void setDatabaseRef(String databaseRef) {
        this.databaseRef = databaseRef;
    }

    public String getKondisiSungai() {
        return kondisiSungai;
    }

    public void setKondisiSungai(String kondisiSungai) {
        this.kondisiSungai = kondisiSungai;
    }

    public ArrayList<String> getArrayListRiverActivity() {
        return arrayListRiverActivity;
    }

    public void setArrayListRiverActivity(ArrayList<String> arrayListRiverActivity) {
        this.arrayListRiverActivity = arrayListRiverActivity;
    }

    public String getRiverDescription() {
        return riverDescription;
    }

    public void setRiverDescription(String riverDescription) {
        this.riverDescription = riverDescription;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getRiverLocation() {
        return riverLocation;
    }

    public void setRiverLocation(String riverLocation) {
        this.riverLocation = riverLocation;
    }

    public ArrayList<String> getArrayListLike() {
        return arrayListLike;
    }

    public void setArrayListLike(ArrayList<String> arrayListLike) {
        this.arrayListLike = arrayListLike;
    }

    public String getUuidUserPhoto() {
        return uuidUserPhoto;
    }

    public void setUuidUserPhoto(String uuidUserPhoto) {
        this.uuidUserPhoto = uuidUserPhoto;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
