package com.baskara.geotaggingsungai;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder>{
    private List<Timeline> homeList;
    private StorageReference mStorage = FirebaseStorage.getInstance().getReference();
    private StorageReference imageRef;
    private Context context;
    OnBottomReachedListener onBottomReachedListener;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewPostedBy, textViewRiverLocation, likeCount, textViewLikeCount, textViewRiverCondition, textViewRiverDescription
                ,commentCount, textViewRiverActivity, timeStamp;
        public ImageView imageViewRiver, commentIcon;
        public CheckBox likeIcon;
        public CircleImageView circleImageViewProfile;
        public MyViewHolder(View view) {
            super(view);
            timeStamp = view.findViewById(R.id.timeStampHome);
            textViewPostedBy = view.findViewById(R.id.textViewPostedByHome);
            textViewRiverLocation = view.findViewById(R.id.textViewRiverLocationHome);
            textViewRiverActivity = view.findViewById(R.id.textViewRiverActivityHome);
            likeCount = view.findViewById(R.id.likeCountHome);
            textViewLikeCount = view.findViewById(R.id.textViewLikeCountHome);
            textViewRiverCondition = view.findViewById(R.id.textViewRiverConditionHome);
            textViewRiverDescription = view.findViewById(R.id.textViewRiverDescriptionHome);
            commentCount = view.findViewById(R.id.commentCountHome);
            imageViewRiver = view.findViewById(R.id.imageViewRiverHome);
            commentIcon = view.findViewById(R.id.commentIconHome);
            likeIcon = view.findViewById(R.id.likeIconHome);
            circleImageViewProfile = view.findViewById(R.id.circleImageViewProfileHome);
        }
    }
    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener){
        this.onBottomReachedListener = onBottomReachedListener;
    }
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_list_row, parent, false);
        return new MyViewHolder(itemView);
    }
    private String getUserID(){
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        String userID = currentFirebaseUser.getUid();
        return userID;
    }
    @Override
    public void onBindViewHolder(final HomeAdapter.MyViewHolder holder, int position) {
        Timeline timeline = homeList.get(position);
        showRiverActivities(timeline, holder);
        holder.textViewRiverDescription.setText(timeline.getRiverDescription());
        holder.textViewRiverCondition.setText(timeline.getKondisiSungai());
        DatabaseReference commentDatabaseRef = FirebaseDatabase.getInstance().getReferenceFromUrl(timeline.getDatabaseRef());
        commentDatabaseRef.child("comment").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int size = (int) dataSnapshot.getChildrenCount();
                if (size==0){
                    holder.commentCount.setText("No comment yet");
                }
                else if (size<2){
                    holder.commentCount.setText("View "+size+" Comment");
                }
                else{
                    holder.commentCount.setText("View "+size+" Comments");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        if (timeline.getArrayListLike()!=null){
            if (timeline.getArrayListLike().size() < 2){
                holder.textViewLikeCount.setText(timeline.getArrayListLike().size()+" Like");
            }
            else{
                holder.textViewLikeCount.setText(timeline.getArrayListLike().size()+" Likes");
            }
        }
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReferenceFromUrl(timeline.getDatabaseRef());
        Log.d("okokok", ref.toString());
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild("arrayListLike")){
                    ArrayList<String> arrayList = (ArrayList<String>) dataSnapshot.child("arrayListLike").getValue();
                    if (arrayList.contains(getUserID())){
                        holder.likeIcon.setChecked(true);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        holder.textViewRiverLocation.setText(timeline.getRiverLocation());
        holder.textViewPostedBy.setText(timeline.getUsername());
        if (timeline.getUuid()!=null){
            if (timeline.getUuid().equalsIgnoreCase("default")){
                holder.imageViewRiver.setImageResource(R.drawable.default_picture);
            }
            else{
                imageRef = mStorage.child(timeline.getUuid());
                imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Uri url = uri;
                        Picasso.with(context).load(url).into(holder.imageViewRiver);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                    }
                });
            }
        }
        if (timeline.getUuidUserPhoto()!=null){
            if (timeline.getUuidUserPhoto().equalsIgnoreCase("default")){
                holder.circleImageViewProfile.setImageResource(R.drawable.default_picture);
            }
            else{
                imageRef = mStorage.child(timeline.getUuidUserPhoto());
                imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Uri url = uri;
                        Picasso.with(context).load(url).resize(355,250).into(holder.circleImageViewProfile);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                    }
                });
            }
        }
        showTimeStamp(timeline.getTimeStamp(), holder);
    }
    private void showRiverActivities(Timeline timeline, MyViewHolder holder){
        String activities = "";
        if (timeline.getArrayListRiverActivity()!= null){
            for (int i=0; i < timeline.getArrayListRiverActivity().size(); i++){
                activities += timeline.getArrayListRiverActivity().get(i)+" . ";
            }
            holder.textViewRiverActivity.setText(activities);
        }
    }
    private void showTimeStamp(Map<String, Object> map, MyViewHolder holder){
        String time = map.get("timestamp").toString();
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(Long.parseLong(time));
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        holder.timeStamp.setText(date);
    }
    @Override
    public int getItemCount() {
        return homeList.size();
    }

    public HomeAdapter(List<Timeline> homeList, Context context) {
        this.homeList = homeList;
        this.context = context;
    }
}
