package com.baskara.geotaggingsungai;

public class UuidPhoto {
    public String currentUuid;
    public String newUuid;

    public UuidPhoto(String currentUuid, String newUuid) {
        this.currentUuid = currentUuid;
        this.newUuid = newUuid;
    }

    public String getCurrentUuid() {
        return currentUuid;
    }

    public String getNewUuid() {
        return newUuid;
    }
}
